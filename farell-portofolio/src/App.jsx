import React from "react";
import { MatrixEffect } from "./components";

const App = () => {
  return (
    <div>
      <MatrixEffect />
    </div>
  );
};

export default App;
