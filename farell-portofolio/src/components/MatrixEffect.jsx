import React, { useEffect, useRef, useState } from "react";

const MatrixRainingCode = () => {
  const canvasRef = useRef(null);

  const symbol = (isMobileDevice) => {
    // to check if its mobile or not
    const canvas = canvasRef.current;
    const ctx = canvas.getContext("2d");
    let width = (canvas.width = window.innerWidth);
    let height = (canvas.height = window.innerHeight);
    let font = isMobileDevice ? 12 : 18;
    let columns = Math.floor(width / font); // Number of columns based on character width
    const characters =
      "アァカサタナハマヤャラワガザダバパイィキシチニヒミリヰギジヂビピウゥクスツヌフムユュルグズブヅプエェケセテネヘメレヱゲゼデベペオォコソトノホモヨョロヲゴゾドボポヴッンㄱㄴㄷㄹㅁㅂㅅㅈㅊㅋㅌㅍㅎㅏㅑㅓㅕㅗㅜㅠㅡ0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let drops = [];

    // Initialize drops
    for (let i = 0; i < columns; i++) {
      drops[i] = Math.floor(Math.random() * height);
    }

    const draw = () => {
      // Create a translucent black rectangle to create the fading effect
      ctx.fillStyle = "rgba(0, 0, 0, 0.04)";
      ctx.fillRect(0, 0, width, height);

      ctx.fillStyle = "#0f0"; // Green color for characters
      ctx.textAllign = "center";

      // Draw the characters
      ctx.font = font + "px monospace";
      for (let i = 0; i < drops.length; i++) {
        const text = characters.charAt(
          Math.floor(Math.random() * characters.length)
        );
        ctx.fillText(text, i * font, drops[i] * font);

        // Reset drops when it reaches the bottom of the canvas
        if (drops[i] * font > height && Math.random() > 0.975) {
          drops[i] = 0;
        }

        drops[i]++;
      }
    };
    let lastTime = 0;
    const fps = 27;
    const nextFrame = 1000 / fps;
    let timer = 0;

    const animate = (timeStamp) => {
      let deltaTime = timeStamp - lastTime;
      lastTime = timeStamp;
      if (timer > nextFrame) {
        draw();
        timer = 0;
      } else {
        timer += deltaTime;
      }
      // Update the animation only if enough time has passed
      requestAnimationFrame(animate);
    };
    animate(0);

    // Update canvas dimensions on window resize
    const handleResize = () => {
      width = canvas.width = window.innerWidth;
      height = canvas.height = window.innerHeight;
      columns = Math.floor(width / font);
      drops = [];
      for (let i = 0; i < columns; i++) {
        drops[i] = 1;
      }
    };
  };

  useEffect(() => {
    const isMobileDevice = /Mobi/i.test(window.navigator.userAgent);
    symbol(isMobileDevice);
  }, []);

  return (
    <canvas
      className="matrix-canvas fixed top-0 left-0 z-[-1] bg-black"
      ref={canvasRef}
    ></canvas>
  );
};

export default MatrixRainingCode;
